package payload

type SMSAggregator struct {
	Response interface{} `json:"response"`
}

type SMSCreateRequest struct {
	Phone    string `json:"phone" valid:"required"`
	Message  string `json:"message" valid:"required"`
	Endpoint string `json:"endpoint" valid:"required"`
}
