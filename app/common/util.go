package common

import (
	dataHttp "gitlab.com/milhamrk/sms-services/app/http"
)

func GetPaginationFields() dataHttp.PaginationFields {
	return dataHttp.PaginationFields{
		QueryField:  "search",
		SortField:   "order",
		LimitField:  "page_size",
		OffsetField: "page",
	}
}
