package common

import (
	"errors"
	"net/http"

	phttp "gitlab.com/milhamrk/sms-services/app/http"
)

var ErrMissingAuthorizationHeader = errors.New("Missing Authorization Header")
var ErrInvalidRequest = errors.New("Invalid request")
var ErrCreateData = errors.New("Error on create data")
var ErrDataExist = errors.New("Data exist")
var ErrSlugExist = errors.New("Slug already used")
var ErrUpdateData = errors.New("Error on update data")
var ErrDeleteData = errors.New("Error on delete data")
var ErrNotFoundData = errors.New("Error not found data")
var ErrParamEndpointNotFound = errors.New("Error endpoint parameter not found in system")

func InjectErrors(handlerCtx *phttp.HttpHandlerContext) {
	handlerCtx.AddError(ErrMissingAuthorizationHeader, setErrResp(ErrMissingAuthorizationHeader.Error(), http.StatusNonAuthoritativeInfo))
	handlerCtx.AddError(ErrInvalidRequest, setErrResp(ErrInvalidRequest.Error(), http.StatusBadRequest))
	handlerCtx.AddError(ErrCreateData, setErrResp(ErrCreateData.Error(), http.StatusBadGateway))
	handlerCtx.AddError(ErrSlugExist, setErrResp(ErrSlugExist.Error(), http.StatusBadGateway))
	handlerCtx.AddError(ErrDataExist, setErrResp(ErrDataExist.Error(), http.StatusConflict))
	handlerCtx.AddError(ErrUpdateData, setErrResp(ErrUpdateData.Error(), http.StatusBadRequest))
	handlerCtx.AddError(ErrDeleteData, setErrResp(ErrDeleteData.Error(), http.StatusBadRequest))
	handlerCtx.AddError(ErrNotFoundData, setErrResp(ErrNotFoundData.Error(), http.StatusNotFound))
	handlerCtx.AddError(ErrParamEndpointNotFound, setErrResp(ErrParamEndpointNotFound.Error(), http.StatusNotFound))
}

func setErrResp(message string, statusCode int) *phttp.ErrorResponse {
	return &phttp.ErrorResponse{
		Response: phttp.Response{
			ResponseDesc: message,
		},
		HttpStatus: statusCode,
	}
}
