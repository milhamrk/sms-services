package handler

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/milhamrk/sms-services/app/common"
	dataHttp "gitlab.com/milhamrk/sms-services/app/http"
	"gitlab.com/milhamrk/sms-services/app/payload"
	"gitlab.com/milhamrk/sms-services/app/service"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type HandlerOption struct {
	*common.Option
	Service *service.Service
}

type smsHandler struct {
	*HandlerOption
}

func NewSMSAggregatorHandler(opt *HandlerOption) *smsHandler {
	return &smsHandler{
		HandlerOption: opt,
	}
}

// SMSAggregator godoc
// @Summary Show a SMSAggregator
// @Description POST a SMSAggregator
// @ID sms-services-show
// @Accept json
// @Produce json
// @Tags SMSAggregator
// @Param requestBody body payload.SMSCreateRequest true "Request body"
// @Success 200 {object} dataHttp.SuccessResponse{data=payload.SMSAggregator}
// @Failure 401 {object} dataHttp.ErrorResponse
// @Failure 500 {object} dataHttp.ErrorResponse
// @Failure default {object} dataHttp.ErrorResponse
// @Router /send-message [post]
func (h *smsHandler) SendMessage(w http.ResponseWriter, r *http.Request) (response dataHttp.HttpHandleResult) {

	// Read from request body
	var reqParam payload.SMSCreateRequest
	err := json.NewDecoder(r.Body).Decode(&reqParam)
	if err != nil {
		response.Error = common.ErrInvalidRequest
		return
	}

	data, err := h.Service.SMSAggregator.SendMessage(&h.Config.Url, &r.Header, reqParam)
	if err != nil || data == nil {
		h.Logger.Error().Msgf("Data not found")
		response.Error = err
		return
	}

	response.StatusCode = http.StatusOK
	response.Data = data
	return
}

// SMSAggregator godoc
// @Summary Change Vendor a SMSAggregator
// @Description Change Vendor a SMSAggregator
// @ID sms-services-change-vendor
// @Accept json
// @Produce json
// @Tags SMSAggregatorVendor
// @Success 200 {object} dataHttp.SuccessResponse{data=payload.SMSAggregator}
// @Failure 401 {object} dataHttp.ErrorResponse
// @Failure 500 {object} dataHttp.ErrorResponse
// @Failure default {object} dataHttp.ErrorResponse
// @Router /change-vendor/{index} [get]
func (h *smsHandler) ChangeVendor(w http.ResponseWriter, r *http.Request) (response dataHttp.HttpHandleResult) {

	// Get path param, order stock_code
	index := chi.URLParam(r, "index")
	if index == "" {
		//random URL_DEFAULT
		lengthNomor := len(strings.Split(h.Config.Url.Nomor, ","))
		rand.Seed(time.Now().UTC().UnixNano())
		index = strconv.Itoa(rand.Intn(lengthNomor) + 1)
	}

	indexNumber, err := strconv.ParseUint(index, 10, 32)
	data, err := h.Service.SMSAggregator.ChangeVendor(&h.Config.Url, int(indexNumber))
	if err != nil || data == nil {
		h.Logger.Error().Msgf("Data not found")
		response.Error = err
		return
	}

	response.StatusCode = http.StatusOK
	response.Data = data
	return
}
