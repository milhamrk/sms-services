package handler

import (
	"net/http"

	dataHttp "gitlab.com/milhamrk/sms-services/app/http"
)

type healthCheckHandler struct {
	*HandlerOption
}

func NewHealthCheckHandler(opt *HandlerOption) *healthCheckHandler {
	return &healthCheckHandler{
		HandlerOption: opt,
	}
}

// HealthCheck godoc
// @Summary HealthCheck Readiness
// @Description HealthCheck Readiness
// @ID health-check
// @Accept json
// @Produce json
// @Tags Health Check
// @Success 200 {object} dataHttp.SuccessResponse{data=payload.HealthCheck}
// @Failure 401 {object} dataHttp.ErrorResponse
// @Failure 500 {object} dataHttp.ErrorResponse
// @Failure default {object} dataHttp.ErrorResponse
// @Router /v1/health/readiness [get]
func (h *healthCheckHandler) Readiness(w http.ResponseWriter, r *http.Request) (response dataHttp.HttpHandleResult) {
	ctx := r.Context()

	err := h.Service.HealthCheck.SimpleHealthCheck(ctx)
	if err != nil {
		response.Error = err
		return
	}

	response.Data = struct {
		HealthCheck string `json:"health_check"`
	}{HealthCheck: "up"}

	response.IsPlainResponse = true

	return
}
