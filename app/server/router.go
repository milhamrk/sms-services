package server

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/milhamrk/sms-services/app/common"
	"gitlab.com/milhamrk/sms-services/app/handler"
	smsAggregator "gitlab.com/milhamrk/sms-services/app/handler"
	phttp "gitlab.com/milhamrk/sms-services/app/http"
)

func Router(opt *handler.HandlerOption) *chi.Mux {
	handlerCtx := phttp.NewContextHandler(opt.Config.Log.IsDebug())

	common.InjectErrors(&handlerCtx)

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Recoverer)
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token", "X-User-Id", "X-User-Membership", "X-User-Name"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300,
	}))

	phandler := phttp.NewHttpHandler(handlerCtx)
	// All new handler/controller assigned here...
	smsAggregatorHandler := smsAggregator.NewSMSAggregatorHandler(opt)

	healthCheckHandler := handler.NewHealthCheckHandler(opt)

	r.Route("/health", func(r chi.Router) {
		r.Method(http.MethodGet, "/readiness", phandler(healthCheckHandler.Readiness))
	})

	r.Route("/", func(r chi.Router) {
		r.Method(http.MethodPost, "/send-message", phandler(smsAggregatorHandler.SendMessage))
		r.Method(http.MethodGet, "/change-vendor/{index}", phandler(smsAggregatorHandler.ChangeVendor))
		r.Method(http.MethodGet, "/change-vendor", phandler(smsAggregatorHandler.ChangeVendor))
	})

	return r
}
