package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"gitlab.com/milhamrk/sms-services/app/common"
	"gitlab.com/milhamrk/sms-services/app/handler"
	"gitlab.com/milhamrk/sms-services/app/service"
)

type IServer interface {
	Start()
}

type server struct {
	*common.Option
	Service *service.Service
}

func NewServer(opt *common.Option, svc *service.Service) IServer {
	return &server{
		Option:  opt,
		Service: svc,
	}
}

func (s *server) Start() {
	var srv http.Server
	idleConnectionClosed := make(chan struct{})

	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		s.Logger.Info().Msg("Server is shutting down")

		if err := srv.Shutdown(context.Background()); err != nil {
			s.Logger.Error().Err(err).Msg("Server failed to shut down")
		}

		close(idleConnectionClosed)
	}()

	srv.Addr = fmt.Sprintf("%s:%s", s.Config.Server.Host, s.Config.Server.Port)
	hOpt := &handler.HandlerOption{
		Option:  s.Option,
		Service: s.Service,
	}
	srv.Handler = Router(hOpt)

	s.Logger.Info().Msgf("Server is starting at %s", srv.Addr)

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		s.Logger.Error().Err(err).Msg("Server failed to start")
	}

	<-idleConnectionClosed
	s.Logger.Info().Msg("Server shutted down")
}
