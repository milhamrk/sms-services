package service

import (
	"gitlab.com/milhamrk/sms-services/app/common"
)

type ServiceOption struct {
	*common.Option
}

type Service struct {
	HealthCheck        IHealthCheck
	SMSAggregator ISMSAggregatorService
}
