package service

import (
	"bytes"
	"encoding/json"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	"gitlab.com/milhamrk/sms-services/app/common"
	"gitlab.com/milhamrk/sms-services/app/payload"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type ISMSAggregatorService interface {
	SendMessage(*common.Url, *http.Header, payload.SMSCreateRequest) (*payload.SMSAggregator, error)
	ChangeVendor(url *common.Url, index int) (*payload.ChangeVendorResponse, error)
}

type smsAggregatorService struct {
	*ServiceOption
}

func NewSMSAggregatorService(opt *ServiceOption) ISMSAggregatorService {

	return &smsAggregatorService{
		opt,
	}
}

func (e *smsAggregatorService) SendMessage(url *common.Url, header *http.Header, reqParam payload.SMSCreateRequest) (payloadHome *payload.SMSAggregator, err error) {
	home := payload.SMSAggregator{}
	selectEndpoint := strings.Split(reqParam.Endpoint, "endpoint")
	var indexString string

	if len(selectEndpoint) <= 1 {
		//if URL_DEFAULT no value
		env, _ := godotenv.Read()
		if env["URL_DEFAULT"] == "" {
			lengthNomor := len(strings.Split(url.Nomor, ","))
			rand.Seed(time.Now().UTC().UnixNano())
			randNomor := rand.Intn(lengthNomor) + 1

			e.ChangeVendor(url, randNomor)
			indexString = strconv.Itoa(randNomor)
		} else {
			indexString = env["URL_DEFAULT"]
		}
	} else {
		indexString = selectEndpoint[1]
	}

	index, err := strconv.Atoi(indexString)
	if err != nil {
		// handle error
		return &home, err
	}

	if len(strings.Split(url.Nomor, ",")) < index {
		// handle error
		return &home, common.ErrParamEndpointNotFound
	}
	urlNomor := strings.Split(url.Nomor, ",")[index-1]
	token := strings.Split(url.Token, ",")[index-1]
	//
	//call endpoint
	//

	var jsonStr = []byte(`{"phone":"` + reqParam.Phone + `","message":"` + reqParam.Message + `"}`)

	end_point := urlNomor
	req, err := http.NewRequest("POST", end_point, bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Error().Msgf("Error : %v", err)
	}

	req.Header.Add("Authorization", token)
	req.Header.Add("Content-Type", "application/json")

	log.Debug().Msgf("Endpoint: %v", end_point)
	response, err := http.DefaultClient.Do(req)
	log.Debug().Msgf("Debug: %v", response)
	if err != nil {
		log.Error().Msgf("Error : %v", err)
	}

	body := map[string]interface{}{}
	decode := json.NewDecoder(response.Body)
	if decode == nil {
		log.Error().Msgf("Error : %v", "error decoder")
		home.Response = []string{}
	}

	err_decode := decode.Decode(&body)
	if err_decode != nil {
		log.Error().Msgf("Error : %v", err_decode)
		home.Response = []string{}
	}

	if body == nil {
		home.Response = []string{}
	} else {
		home.Response = body
	}

	return &home, nil
}

func (e *smsAggregatorService) ChangeVendor(url *common.Url, index int) (*payload.ChangeVendorResponse, error) {
	godotenv.Load()

	response := payload.ChangeVendorResponse{}

	if len(strings.Split(url.Nomor, ",")) < index || index <= 0 {
		// handle error
		return &response, common.ErrParamEndpointNotFound
	}

	env, _ := godotenv.Read()
	env["URL_DEFAULT"] = strconv.Itoa(index)
	godotenv.Write(env, "./.env")

	log.Debug().Msgf("URL_DEFAULT : %v", strconv.Itoa(index))
	response.Response = "Success change default vendor" + strconv.Itoa(index)
	return &response, nil
}
