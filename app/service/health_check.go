package service

import "context"

type IHealthCheck interface {
	SimpleHealthCheck(ctx context.Context) error
}

type healthCheckService struct {
	*ServiceOption
}

func NewHealthCheckService(opt *ServiceOption) IHealthCheck {
	return &healthCheckService{
		ServiceOption: opt,
	}
}

func (s *healthCheckService) SimpleHealthCheck(ctx context.Context) error {
	return nil
}