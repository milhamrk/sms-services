package main

import (
	"gitlab.com/milhamrk/sms-services/cmd"
)

// @title SMS Aggregator
// @version 1.0
// @description SMS Aggregator Service.
// @termsOfService http://swagger.io/terms/

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8081
// @query.collection.format SMS Aggregator

func main() {
	cmd.Execute()
}
