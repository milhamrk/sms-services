FROM asia.gcr.io/emtrade-v2/debian-base:latest

ARG APP_NAME
ENV APP_NAME=$APP_NAME

WORKDIR /go/bin

# Import static file
COPY bin/${APP_NAME} /go/bin/${APP_NAME}
# COPY migrations /go/bin/migrations

# Use an unprivileged user.
RUN adduser --disabled-password --gecos '' ${APP_NAME}
USER ${APP_NAME}

# Run the generated shell script.
ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD /go/bin/${APP_NAME}