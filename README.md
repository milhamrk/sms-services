# sms-services
sms-services service is a service management to manage another service to get a data.

Sample Request

POST /send-message
```
{
    "phone" : "628881104996",
    "message" : "-Dev Test"
}
```

GET /change-vendor/{index}

Here I use Whatsapp Service for this case, send to number with select endpoint from ENV Array config. endpoint1 for URL index 0, endpoint2 for url index 1, and more.

For temporary I use godot for running change of vendor. 
Soon I will implement feature flag toggle using Consul/Unleash

## Sequence Diagram
![sms-services-diagram](docs/sequence.png)

1. Go to your Gitlab.com account's profile and navigate to `SSH Keys` [menu](https://gitlab.com/-/profile/keys). Add new SSH key from your `~/.ssh/id_rsa.pub`, if not exists, please see [here](https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key) to generate a new one.
2. Setup your Git's config, open `~/.gitconfig` then add this at end of file
```shell
[url "git@gitlab.com:"]
    insteadOf = https://gitlab.com/
```
3. Set `GOPRIVATE` environment variables on your shell, by placing it on `~/.bashrc` or `~/.zshrc` depends on which shell that you use.
```shell
GOPRIVATE="gitlab.com/milhamrk/*"
```
4. Reload your shell
```shell
$ $SHELL
```
## Requirement
1. Go 1.15 or above (download [here](https://golang.org/dl/)).
2. Git (download [here](https://git-scm.com/downloads)).
3. Docker
4. Swag for swagger documentation (download [here](https://github.com/swaggo/swag))
5. Mermaid for rendering sequence diagram [mermaid page](https://mermaid-js.github.io/mermaid/#/)
6. K6 for running load test [K6 page](https://k6.io)
## How To Setup
### Project
clone the source locally:
```shell
$ git clone git@gitlab.com:milhamrk/sms-services.git
$ cd sms-services
```
Copy the example env file and make the required configuration changes in the `app.env` file
```shell
$ cp .env.example .env
```
Start the local development server
```shell
$ make run
```
You can now access the server at http://localhost:8081
### Database
You can run `docker-compose up -d` on root project to running the database (optional) and run command `make run ARGS=migrate` to migrate database table.
## How To Use
We are encouraging makefile to operate and deploy sms-services. Each command has its own scope & roles.
Use `make help` to see complete command and details.

- Build, `make build`
- Run sms-services, `make run`
- Run a DB migration, `make run ARGS=migrate`
- Rollback DB migration, `make run ARGS=migratedown`
- Create OpenAPI spec file, `make swagger`
- Create sequence diagram, `make mermaid`
- Run unit test, `make test`
## How to contribute
1. Create your feature branch (`git checkout -b [feat/fix/ref]-foo-bar`)
2. Commit your changes (`git commit -am 'feat: add some fooBar'`)
3. Push to the branch (`git push origin [feat/fix/ref]-foo-bar`)
4. Create a new Merge Request, please use this template when submitting a MR