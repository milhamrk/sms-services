package cmd

import (
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/milhamrk/sms-services/app/common"
	"gitlab.com/milhamrk/sms-services/app/server"
	"gitlab.com/milhamrk/sms-services/app/service"
)

func startHttpServer() {
	conf, err := common.NewConfig()
	if err != nil {
		log.Error().Msgf("Config error | %v", err)
		panic(err)
	}

	logger := common.NewLogger(conf.Log)

	options := &common.Option{
		Config: conf,
		Logger: logger,
	}

	services := wiringServerService(&service.ServiceOption{
		Option: options,
	})

	qu := server.NewServer(options, services)
	qu.Start()

}

var RootCmd = &cobra.Command{
	Use:   "root",
	Short: "SMSAggregator",
	Long:  `SMSAggregator service`,
	Run: func(cmd *cobra.Command, args []string) {
		startHttpServer()
	},
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func wiringServerService(opt *service.ServiceOption) *service.Service {
	smsAggregator := service.NewSMSAggregatorService(opt)
	healthCheck := service.NewHealthCheckService(opt)

	return &service.Service{
		SMSAggregator: 		smsAggregator,
		HealthCheck:        healthCheck,
	}
}
